
# STORESHED

### Зачем?

Для контроля ререндеров компонентов при изменении глобального стейта. Данный стейт менеджер позволяет полностью контролировать количество ререндеров компонентов. 

### Как пользоваться?

1. Создаем хранилища и объединяем их в один объект. Этот объект будет передоваться в экшены.

store.ts
```typescript
import { Store } from 'storeshed';

interface IState1 {
    a: string;
    b: number;
}

interface IState2 {
    a: string;
    b: number;
}
export const store1 = new Store<IState1>({a: '', b: 4});

export const useStore1 = (key: keyof IState1) => useStore(store1, key);
// ...
// another stores
// ...
export const mainStore = {
    store1,
    // another stores
}

export type TMainStore = typeof mainStore;
```

2. Создаем экшены с помощью bindActions.

actions.ts
```typescript
import { bindActions } from 'storeshed';

const createActions = () => {
    const add = ({store1, store2}: TMainStore, num: number) => {
        store1.dispatch('a', store.get('a') + num);
    }
    //...
    // some other actions
    //...
    return {
        add,
        //other actions
    }
}

//...
// some action creators
//...
export const actions = bindActions(mainStore, createActions());
```

3. Используем все выше описанное в компоненте.

SomeComponent.tsx
```typescript
export const SomeComponent: React.FC = () => {
    const num = useStore1('a');

    return ( 
        <button onClick={()=> {actions.add(5)}}>
            { num }
        </button>
    );
}
```