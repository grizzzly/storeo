import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { createGlobalStyle } from "styled-components";

const MainPage = lazy(() => import("./pages/MainPage"));

export const App: React.FC = () => {
  return (
    <Router>
      <$GlobalStyles />
      <MainPage />
    </Router>
  );
};

const $GlobalStyles = createGlobalStyle`
* {
  margin: 0;
  padding: 0;
  outline: none;
  box-sizing: border-box;
  font-family: sans-serif;
}

html,
body {
  position: relative;
  width: 100%;
  margin: 0;
}
`;
