import React, { useEffect } from "react";
import {
  Store,
  createStoreContext,
  useStore,
  useStoreComputed,
} from "../../../src";
import {
  actions1,
  actions2,
  store1,
  store2,
  useStore1,
  useStore1Computed,
} from "../../store";

declare global {
  interface Window {
    store: Store<any>;
  }
}

export default function MainPage() {
  console.log("Main render");

  return (
    <>
      <Store1View />
      <Store2View />
    </>
  );
}

const Store1View = () => {
  const title = useStore1("title");
  const num = useStore1Computed("num", (state) => "" + state.num);
  console.log("Store 1 view render");

  return (
    <div>
      <button
        onClick={() => {
          actions1.inc();
        }}
      >
        click
      </button>
      <p
        onClick={() => {
          actions1.updateTitle();
        }}
      >
        {title}
      </p>
      <p>{num}</p>
    </div>
  );
};

const Store2View = () => {
  const title = useStore(store2, "title");
  const num = useStore(store2, "num");
  console.log("Store 2 view render");

  return (
    <div>
      <button
        onClick={() => {
          actions2.inc();
        }}
      >
        click
      </button>
      <p>{title}</p>
      <p>{num}</p>
    </div>
  );
};
