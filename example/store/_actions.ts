import { bindActions } from "../../src";
import { TMainStore, mainStore } from "./_mainStore";

export const createActions1 = () => {
  const inc = ({ store1, store2 }: TMainStore) => {
    store1.dispatch("num", store1.get("num") + 1);
  };

  const updateTitle = ({ store1, store2 }: TMainStore) => {
    store2.dispatch(
      "title",
      store2.get("title") + store1.get("num") + store2.get("num")
    );
    return 1;
  };

  return {
    inc,
    updateTitle,
  };
};

export const createActions2 = () => {
  const inc = ({ store1, store2 }: TMainStore) => {
    store2.dispatch("num", store1.get("num") + 1);
  };

  const updateTitle = ({ store1, store2 }: TMainStore) => {
    store1.dispatch(
      "title",
      store2.get("title") + store1.get("num") + store2.get("num")
    );
  };
  return {
    inc,
    updateTitle,
  };
};

export const actions1 = bindActions(mainStore, createActions1());
export const actions2 = bindActions(mainStore, createActions2());
