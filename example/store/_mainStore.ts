import { Store, useStore, useStoreComputed } from "../../src";

export interface IStoreState {
  title: string;
  num: number;
}

export const store1 = new Store({
  title: "Store1",
  num: 1,
});

export const store2 = new Store({
  title: "Store2",
  num: 1,
});

export const mainStore = {
  store1,
  store2,
};

export const useStore1 = <K extends keyof IStoreState>(key: K) =>
  useStore(store1, key);

export const useStore1Computed = <K extends keyof IStoreState, R>(
  key: K,
  cb: (state: IStoreState) => R
): R => useStoreComputed(store1, key, cb);

export const useStore2 = <K extends keyof IStoreState>(key: K) =>
  useStore(store2, key);

export type TMainStore = typeof mainStore;
