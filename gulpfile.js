const { series, src } = require('gulp');
var gClean = require('gulp-clean');
var run = require('gulp-run-command').default;

function clean(cb) {
  src('./lib', { read: false, allowEmpty: true }).pipe(gClean());
  src('./tsconfig.tsbuildinfo', { read: false, allowEmpty: true }).pipe(
    gClean()
  );
  cb();
}

exports.default = clean;
