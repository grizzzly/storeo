export type ICallback<T> = (value: T, prevValue: T) => void;

type ISubs<S extends {}> = {
  [K in keyof S]: ICallback<S[K]>[];
};
type IGroupSubs<S extends {}> = {
  [K in keyof S]: ((state: S) => void)[];
};

/**
 * абстрактное хранилище данных
 */
export abstract class AStore<S extends {}> {
  protected state: S; // стэйт;
  private subs: ISubs<S>; // колбеки, привязанные к одному полю
  private groupSubs: IGroupSubs<S>; // колбеки, привязанные к нескольким полям
  private stateSubs: ((state: S) => void)[] = [];
  /**
   * @param initState - изначальные значения стэйта
   */
  constructor(initState: S) {
    this.state = initState;
    const subs: Partial<ISubs<S>> = {};
    const groupSubs: Partial<IGroupSubs<S>> = {};

    (Object.keys(initState) as (keyof S)[]).forEach((k) => {
      subs[k] = [];
      groupSubs[k] = [];
    });
    this.subs = subs as ISubs<S>;
    this.groupSubs = groupSubs as IGroupSubs<S>;
  }

  get();
  get<K extends keyof S>(key: K): S[K];
  get(key?) {
    return key === undefined ? { ...this.state } : this.state[key];
  }

  abstract dispatch(...args: any): any;

  protected updateOne<K extends keyof S>(key: K, value: S[K]) {
    const prevValue = this.state[key];

    if (prevValue === value) return;

    this.state[key] = value;

    this.notifyStateSubs();
    this.notify(key as keyof S, value, prevValue);
  }

  protected updateMany(params: Partial<S>) {
    const prevState = { ...this.state };

    this.state = {
      ...this.state,
      ...params,
    };

    this.notifyStateSubs();

    (Object.keys(params) as (keyof S)[]).forEach((k) => {
      const prevValue = prevState[k];
      this.notify(k as keyof S, params[k] as S[keyof S], prevValue);
    });
  }

  /**
   * Подписка на изменения
   * Можно подписаться на 1 поле, несколько
   * полей или на любое изменение стейта
   * @param {'@state' | keyof S | (keyof S)[]} keys
   * @param cb - коллбэк
   */
  on(keys: "@state", cb: ICallback<S>): () => void;

  on(keys: (keyof S)[], cb: ICallback<S>): () => void;

  on<K extends keyof S>(key: K, cb: ICallback<S[K]>): () => void;

  on(arg: "@state" | keyof S | (keyof S)[], cb: any) {
    if (typeof cb !== "function") {
      throw Error("cb is not a function");
    }

    if (arg === "@state") {
      this.stateSubs.push(cb);
      return () => {
        this.stateSubs = this.stateSubs.filter((f) => f !== cb);
      };
    }

    if (Array.isArray(arg)) {
      arg.forEach((key) => this.groupSubs[key].push(cb));
      return () => {
        arg.forEach((key) => {
          this.groupSubs[key] = this.groupSubs[key].filter((f) => f !== cb);
        });
      };
    }

    if (typeof arg === "string") {
      if (!(arg in this.state)) {
        throw Error(
          `There is no key ${arg} in state. Keys: ${Object.keys(
            this.state
          ).join(", ")}`
        );
      }

      this.subs[arg].push(cb);

      return () => {
        this.subs[arg] = this.subs[arg].filter((f) => f !== cb);
      };
    }

    throw Error(`Wrong typeof key. Expected string, but got: ${typeof arg}`);
  }

  /**
   * уведомляет всех подписчиков об изменениях
   * @param {keyof S} key - ключ
   * @param value - новое значение поля
   * @param prevValue - старое значение поля
   */
  private notify<K extends keyof S>(key: K, value: S[K], prevValue: S[K]) {
    this.notifyGroupSubs(key);
    this.notifySubs(key, value, prevValue);
  }

  private notifySubs<K extends keyof S>(key: K, value: S[K], prevValue: S[K]) {
    this.subs[key].forEach((cb) => cb(value, prevValue));
  }

  private notifyGroupSubs<K extends keyof S>(key: K) {
    this.groupSubs[key].forEach((cb) => cb({ ...this.state }));
  }

  private notifyStateSubs() {
    this.stateSubs.forEach((cb) => cb({ ...this.state }));
  }
}
