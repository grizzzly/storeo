import { AStore } from "./_AStore";

/**
 * Хранилище данных
 */
export class Store<S extends {}> extends AStore<S> {
  dispatch(key: Partial<S>);
  dispatch<K extends keyof S>(key: K, value: S[K]);
  dispatch<K extends keyof S>(key: K | Partial<S>, value?: S[K]) {
    if (typeof key === "object") {
      this.updateMany(key);
      return;
    }

    if (!(key in this.state)) throw Error("wrong params");

    this.updateOne(key, value as S[K]);
  }
}
