type OmitFirstArg<State, F> = F extends (s: State, ...args: infer P) => infer R
  ? (...args: P) => R
  : F;

export type IMappedActions<
  State,
  T extends { [name: string]: (state: State, ...args: any[]) => {} }
> = {
  [K in keyof T]: OmitFirstArg<State, T[K]>;
};

export interface IActions<State> {
  [n: string]: (state: State, ...args) => any;
}

/**
 * пробрасывает первым аргументом Store или
 * объект со стором
 */
export const bindActions = <State extends {}, Actions extends IActions<State>>(
  state: State,
  actions: Actions
): IMappedActions<State, typeof actions> => {
  type MappedActions = IMappedActions<State, Actions>;

  const bindedActions: Partial<MappedActions> = {};

  (Object.keys(actions) as Array<keyof Actions>).forEach((key) => {
    actions[key].bind(state);
    bindedActions[key] = actions[key].bind(null, state) as OmitFirstArg<
      State,
      (typeof actions)[typeof key]
    >;
  });

  return bindedActions as MappedActions;
};
