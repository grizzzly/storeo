import React, { createContext, memo } from "react";
import { Store } from "./_Store";
import { useStore } from "./_useStore.hook";

type IContextProps<S extends {}> = {
  store: Store<S>;
};

export function createStoreContext<P, S extends {}>(state: S) {
  const store = new Store(state);

  const Context = createContext({} as IContextProps<typeof state>);

  const Provider: React.FC<P & { children: React.ReactNode }> = memo(
    (props) => {
      return (
        <Context.Provider value={{ store }} {...props}>
          {props.children}
        </Context.Provider>
      );
    }
  );

  const useContextStore = <K extends keyof typeof state>(key: K) =>
    useStore(store, key);

  return {
    store,
    Provider,
    useContextStore,
    dispatch: store.dispatch.bind(store),
    Consumer: Context.Consumer,
  };
}
