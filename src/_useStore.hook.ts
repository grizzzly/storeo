import { useEffect, useState } from "react";
import { Store } from "./_Store";

/**
 * Позволяет подписаться на определенный параметр в сторе
 * @param store
 * @param key
 * @returns
 */
export function useStore<S extends {}, K extends keyof S>(
  store: Store<S>,
  key: K
): S[K] {
  const [state, setState] = useState(store.get(key));

  useEffect(() => {
    const unsub = store.on(key, (value) => {
      setState(value);
    });

    return unsub;
  }, []);

  return state;
}

export const useStoreComputed = <S extends {}, T, K extends keyof S, R>(
  store: Store<S>,
  key: K,
  cb: (state: S) => R
): R => {
  const [state, setState] = useState(cb(store.get()));

  useEffect(() => {
    const unsub = store.on(key, (value) => {
      setState(cb(store.get()));
    });

    return unsub;
  }, []);

  return state;
};
